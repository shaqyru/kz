<?
// root_file - определен в корневом index файле
if (root_file != 'yes') die(); // файл будет запускаться только вместе с корневым файлом
$file_to_include = '';
//==================================================================================================
$record_kol = 24;
$showInfoState = true;

if ($getInfoState) {

    if ($title) {
        $curContent = new ContentInfo($menuInfo->getSuffix($lang));
        $curContRes = $curContent->getContentByMenuAndLink($cmi, $title);

        $tmp_id = $curContent->id[0];
        $title_suffix .= ' : ' . $curContent->title[$tmp_id];
    }


    $siteContent = new ContentInfo($menuInfo->getSuffix($lang));
    $siteContent->setLimit($page, $record_kol);
    $contentResearch = $siteContent->getMenuList($cmi, $lang);
    $total_record_nmber = $siteContent->getCount();


    // echo "<br />total_record_nmber = ".$total_record_nmber;
    // echo "<br />str = ".$siteContent->ListRecord->str;

    if ($total_record_nmber == 1) {

        $contentResearch = false;
        if (!$title) {
            $curContent = new ContentInfo($menuInfo->getSuffix($lang));
            $curContent = $siteContent;
            $curContRes = true;
            $title = true;
        }
    }

    if ($curContRes and $menuInfo->isTagsSet()) {
        $vId =     $curContent->id[0];
        $curKeyWords = $curContent->tags[$vId];
    }

    $vid_id = $vId;
} // if (!$siteSide)
unset($getInfoState);
?>

<main class="pt-5 mt-4 default_main__block _container">
    <div class="container mt-4 pt-5">
        <div class="section__tooltips">
            <?= $BreadCrumbStr ?>
        </div>
        <?
        // шахматный порядок
        ?>

        <div class="section__sponsor">
            <div class="section__sponsor__content _container">
                <div class="swiper section__sponsor__swiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor1.jpg" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor2.jpg" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor3.png" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor4.jpg" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor5.png" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor6.jpg" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor7.png" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../img/trust/sponsors/sponsor8.png" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor9.png" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor10.png" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor11.jpeg" alt="" />
                        </div>

                        <div class="swiper-slide">
                            <img src="../../img/trust/sponsors/sponsor12.jpeg" alt="" />
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?php

        if ($title) {
            if ($curContRes) {

                $vId =     $curContent->id[0];


                $curAttachedFile = new UploadedFileInfo;
                $curAttachedFile->getFileListByContentId($vId, $lang);



                echo '<div class="section__article__item__date">';
                // вывод автора если разрешено

                if ($menuInfo->isAuthorSet() and $curContent->author[$vId]) {
                    echo "<div class='section__article__item__date__inf' >" . $text_alert['author'] . ": " . $curContent->author[$vId] . "</div>";
                }

                // вывод тэгов если разрешено
                if ($menuInfo->isViewsSet()) {
                    echo "<div class='section__article__item__date__views'>" . $text_alert['views'] . ": " . $curContent->views[$vId] . "</div>";
                }

                echo '</div>';



                // вывод заголовка если разрешено
                if ($menuInfo->isTitleSet()) {
                    echo '<div class="section__article__item__title">' . $curContent->title[$vId] . '</div>';
                }






                // вывод текста если разрешено
                if ($menuInfo->isTextSet()) {
                    // echo "<p>".$curContent->getText($vId, $curAttachedFile)."</p>";
                    echo "<p class='about_description pb-5'>" . $curContent->getText($vId, $curAttachedFile) . "</p>";
                    // echo "<div style='clear:both; margin-bottom:25px'></div>";
                }



                $index = 'show_gallery';
                $just_gallery = $curContent->more_info[$vId][$index];
                if ($just_gallery) // если был дан указ показать галерею
                {

                    echo '<div class="section__article__item__img">';

                    //--------------------------------------------------------------------------------------------------
                    // Вывод изображении
                    $config_to_show = 'image';
                    if (is_array($curAttachedFile->fileTypeArr[$config_to_show])) // в списке показывать не будем
                    {
                        $img_w = 'width:150px;';
                        echo '<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">';
                        echo '<div class="carousel-inner">';

                        $active = true;
                        foreach ($curAttachedFile->fileTypeArr[$config_to_show] as $kId => $vFlId) {
                            $root_large_src = $_SERVER['DOCUMENT_ROOT'] . $img_src . "large_" . $curAttachedFile->name[$vFlId];
                            if (file_exists($root_large_src)) {
                                $large_src = $img_src . "large_" . $curAttachedFile->name[$vFlId];
                                $file_com = stripslashes($curAttachedFile->comment[$vFlId]);
                                $img_title = $file_com ? $file_com : $curContent->title[$vId];

                                echo '<div class="carousel-item' . ($active ? ' active' : '') . '">';
                                echo '<img src="' . $large_src . '" class="d-block w-100" alt="' . $img_title . '">';
                                echo '</div>';

                                $active = false;
                            }
                        }

                        echo '</div>';
                        echo '<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">';
                        echo '<span class="carousel-control-prev-icon" aria-hidden="true"></span>';
                        echo '<span class="visually-hidden">Previous</span>';
                        echo '</button>';
                        echo '<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">';
                        echo '<span class="carousel-control-next-icon" aria-hidden="true"></span>';
                        echo '<span class="visually-hidden">Next</span>';
                        echo '</button>';
                        echo '</div>';
                    }


                    echo '</div>';
                    //--------------------------------------------------------------------------------------------------
                } else {
                    echo "";
                }


                if ($menuInfo->isTimeSet()) {
                    echo "<span class='small_font'>" . $text_alert['show_time'] . ": <span>" . $curContent->time[$vId] . "</span></span> &nbsp;";
                }

                echo '</div>';
                //--------------------------------------------------------------------------------------------------

                $config_to_show = 'mediafiles';
                if (is_array($curAttachedFile->fileTypeArr[$config_to_show])) // в списке показывать не будем
                {
                    $Player = new Player;

                    echo "<ul class='mediafiles_list'>";
                    foreach ($curAttachedFile->fileTypeArr[$config_to_show] as $kId => $vFlId) {
                        echo "<li>";
                        echo "<br />";

                        $extention = $curAttachedFile->extention[$vFlId];
                        $file_src = "/uploads/mediafiles/" . $curAttachedFile->name[$vFlId];
                        $file_com = $curAttachedFile->comment[$vFlId];
                        if ($curAttachedFile->size[$vFlId])
                            $fSize = "(" . $curAttachedFile->size[$vFlId] . ")";
                        else $fSize = "";

                        if ($extention == 'mp3') {

                            echo '
			<audio controls>
				<source src="' . $file_src . '" type="audio/mpeg">
			</audio>
			<a href="' . $file_src . '">Скачать файл</a>
				';
                        }

                        if ($extention == 'flv') {
                            echo "<div align='center'>";
                            $Player->flv($file_src);

                            echo "<br /><br />$file_com";;
                            echo "</div>";
                        }
                        echo "</li>";
                    }
                    echo "</ul>";
                }

                $config_to_show = 'video_link';
                if (is_array($curAttachedFile->fileTypeArr[$config_to_show])) // в списке показывать не будем
                {

                    echo "<ul class='mediafiles_list'>";
                    foreach ($curAttachedFile->fileTypeArr[$config_to_show] as $kId => $vFlId) {
                        echo "<li>";
                        echo htmlspecialchars_decode($curAttachedFile->name[$vFlId]);

                        echo "</li>";
                    }
                    echo "</ul>";
                }
                $config_to_show = 'docfiles';
                if (is_array($curAttachedFile->fileTypeArr[$config_to_show])) {
                    echo '<div class="td_monitoring_otdel mt-5 mb-5" style="width: 80%; margin-left: auto; margin-right: auto;
}">
				<table width="100%" cellspacing="0" cellpadding="0">
				<tbody>
				<tr class="zagolovok" style="background-color:#f5f5f5; color:#e5bc36; font-weight:bold;">
					<td style="padding:10px; border:1px solid grey;">' . $text_alert['td_description'] . '</td>
					<td style="padding:10px; border:1px solid grey;" align="center">' . $text_alert['td_update'] . '</td>
					<td style="padding:10px; border:1px solid grey;" align="center">' . $text_alert['td_download'] . '</td>
				</tr>';
                    foreach (array_reverse($curAttachedFile->fileTypeArr[$config_to_show]) as $kId => $vFlId) {
                        echo "<tr>";
                        if ($curAttachedFile->size[$vFlId])    $fSize = $curAttachedFile->size[$vFlId];
                        else $fSize = '';
                        $docSrc = $ContentList->doc_folder_loc . "/" . $curAttachedFile->name[$vFlId];
                        $comment = $curAttachedFile->comment[$vFlId];
                        $extention = $curAttachedFile->extention[$vFlId];
                        $awesome_file_ext['txt'] = 'file-text-o';
                        $awesome_file_ext_color['txt'] = "gray";
                        $awesome_file_ext['xls'] = 'file-excel-o';
                        $awesome_file_ext_color['xls'] = "green";
                        $awesome_file_ext['xlsx'] = 'file-excel-o';
                        $awesome_file_ext_color['xlsx'] = "green";
                        $awesome_file_ext['doc'] = 'file-word-o';
                        $awesome_file_ext_color['doc'] = "#3185bb";
                        $awesome_file_ext['docx'] = 'file-word-o';
                        $awesome_file_ext_color['docx'] = "#3185bb";
                        $awesome_file_ext['ppt'] = 'file-powerpoint-o';
                        $awesome_file_ext_color['ppt'] = "#d93610";
                        $awesome_file_ext['pptx'] = 'file-powerpoint-o';
                        $awesome_file_ext_color['pptx'] = "#d93610";
                        $awesome_file_ext['pps'] = 'file-powerpoint-o';
                        $awesome_file_ext_color['pps'] = "#d93610";
                        $awesome_file_ext['ppsx'] = 'file-powerpoint-o';
                        $awesome_file_ext_color['ppsx'] = "#d93610";


                        $awesome_file_ext['pdf'] = 'file-pdf-o';
                        $awesome_file_ext_color['pdf'] = "red";
                        $awesome_file_ext['jpg'] = 'file-picture-o';
                        $awesome_file_ext_color['jpg'] = "blue";
                        $awesome_file_ext['bmp'] = 'file-picture-o';
                        $awesome_file_ext_color['bmp'] = "blue";
                        $awesome_file_ext['png'] = 'file-picture-o';
                        $awesome_file_ext_color['png'] = "blue";
                        $awesome_file_ext['gif'] = 'file-picture-o';
                        $awesome_file_ext_color['gif'] = "blue";
                        $awesome_file_ext['arj'] = 'file-archive-o';
                        $awesome_file_ext_color['arj'] = "#fc6b00";
                        $awesome_file_ext['rar'] = 'file-archive-o';
                        $awesome_file_ext_color['rar'] = "brown";
                        $awesome_file_ext['zip'] = 'file-archive-o';
                        $awesome_file_ext_color['zip'] = "#fcac00";
                        $awesome_file_ext['avi'] = 'file-video-o';
                        $awesome_file_ext_color['avi'] = "darkred";
                        $awesome_file_ext['mp3'] = 'file-audio-o';
                        $awesome_file_ext_color['mp3'] = "brown";

                        if (!$awesome_file_ext[$extention]) {
                            $awesome_file_ext[$extention] = 'file';
                            $awesome_file_ext_color[$extention] = "darkblue";
                        }

                        $ext_sign = '<i class="fa fa-' . $awesome_file_ext[$extention] . '" style="color: ' . $awesome_file_ext_color[$extention] . '" aria-hidden="true"></i>';
                        $file_alternate = $text_alert['download'] . " " . strtoupper($extention);

                        $file_src = "/uploads/docfiles/" . $curAttachedFile->name[$vFlId];

                        echo '<td style="border:1px solid grey; padding:15px;" width="50%" align="left"><p>' . $curAttachedFile->comment[$vFlId] . '</p></td>';
                        echo '<td style="border:1px solid grey; padding:15px;" align="center">' . $curAttachedFile->uploadDime[$vFlId] . '</td>';

                        $false_num = UploadedFileInfo::getFalseNum($vFlId);
                        // echo "<td style='border:1px solid grey; padding:15px;' align=\"center\"><a style='font-size:25px;'  href='/download.php?fi_id=" . $false_num . "' alt=\"" . $file_alternate . "\" title=\"" . $file_alternate . "\">" . $ext_sign . "</a>";
                        echo "<td style='border:1px solid grey; padding:15px;' align=\"center\"><a style='font-size:25px;'  href='<?=$file_src?>' alt=\"" . $file_alternate . "\" title=\"" . $file_alternate . "\">" . $ext_sign . "</a>";
                        // echo "<span class='download_style'><a href='/download.php?fi_id=".$false_num."'>Скачать файл</a></span>";
                        echo "<span><a  class='download_button' target='_blank' href=" . $file_src . ">Скачать файл <br> (" . $fSize . ")</a></span>";

                        echo "<br>";
                        // echo "<span style='font-size:12px;'>(" . $fSize . ")</span>";
                        $file_com = '';
                        echo "</td>";


                        //echo "<span class='ico'>".$ext_sign."</span>";
                        //echo "<span class='file_comment'>".$curAttachedFile->comment[$vFlId]."</span> &nbsp;<br />";		  
                        //$false_num = UploadedFileInfo::getFalseNum($vFlId);
                        //echo "<span class='download_style'><a href='/download.php?fi_id=".$false_num."'>Скачать файл</a></span>";
                        //echo "&nbsp;&nbsp;&nbsp;&nbsp;<span class='file_size'>(".strtoupper($extention).", ".$fSize.")</span>"; $file_com ='';

                        //echo "&nbsp;&nbsp;&nbsp;<span class='download_style'><a href='".$file_src."'>".$text_alert['download']."</a></span>";

                        //echo "</li>";
                        echo "</tr>";
                    }
                    echo '</tbody>
			</table>
			</div>';
                    //echo "</ul>";
                }










                //--------------------------------------------------------------------------------------------------






                /*Сертификаты*/
            } // if ($curContRes)


        } // if($title)

        //--------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------

        if ($contentResearch) {
            //print 'List';
            echo '<div class="section__news__inf">';
            foreach ($siteContent->id as $kId => $vId) {

                $attachedFile = new UploadedFileInfo;
                $attachedFile->getFileListByContentId($vId, $lang);




                echo '<div class="page-posts b-bottom">';

                //--------------------
                $config_to_show = 'image';


                // <!--Content Column-->

                $dayweekDate = new FunctionDate;
                $afisha_['date'] = $siteContent->date[$vId];
                $weekd = explode("-", $siteContent->date[$vId]);

                $afisha_['day'] = (int)$weekd[0];
                $afisha_['month'] = (int)$weekd[1];
                $afisha_['year'] = $weekd[2];
                $afisha_['weekday'] = date("w", mktime(0, 0, 0, $weekd[1], $weekd[0], $weekd[2]));
                // $afisha_['month'] = $dayweekDate->DateFormatInfo(2, $afisha_['month'], $lang);

                echo '<div class="content-column">';
                echo '<div class="inner-blog">';
                echo '<div class="container">';
                // main-content
                echo '<div class="section__news__inf__item">';
                echo '<div class="section__news__inf__item__date">' . $afisha_['day'] . '.' . $afisha_['month'] . '.' . $afisha_['year'] . '</div>';
                echo '<div class="section__news__inf__item__title">';
                // вывод даты если разрешено + формат даты
                if ($menuInfo->isDateSet()) {
                    echo '<a class="section__news__inf__item__title" href="?title=' . $siteContent->link[$vId] . '">' . $siteContent->title[$vId] . '</a>';
                }
                // вывод автора если разрешено
                // if ($menuInfo->isAuthorSet() and $siteContent->author[$vId]) {
                //     echo "<li><span  class='icon fa fa-user'><span> " . $text_alert['author'] . ": " . $siteContent->author[$vId] . "</li>";
                // }
                // вывод просмотров если разрешено
                // if ($menuInfo->isViewsSet()) {
                //     echo "<li><span class='icon fa fa-eye'></span> " . $text_alert['views'] . ": " . $siteContent->views[$vId] . "</li>";
                // }
                // echo '<li><span class="icon fa fa-eye"></span> Оқылған саны: '.$LastNews->views[$vId].'</li>';
                echo '</div>';
                echo '<div class="post-text-content about_description">';
                // вывод аннотрации если разрешено
                if ($menuInfo->isAnnotSet() and strlen($siteContent->annot[$vId]) > 10) // Если разрешено и длина аннотации больше N символов
                {
                    echo '<p>' . FunctionStr::cutString(strip_tags($siteContent->annot[$vId]), 150) . ' ...</p>';
                } elseif (1) {
                    echo '<p>';
                    echo FunctionStr::cutString(strip_tags($siteContent->getCleanText($vId)), 150);
                    echo ' ...</p>';
                }
                $goToNews['kz'] = "Жаңалыққа өту";
                $goToNews['ru'] = "Перейти к новости";
                $goToNews['en'] = "Read more";
                echo '<div class="c-area-button-row">';
                echo '<a class="button-row-btn" href="?title=' . $siteContent->link[$vId] . '">' . $goToNews[$lang] . '</a>';
                echo '</div>';
                echo '<p>' . strip_tags(substr($LastNews->text[$vId], 0, 900)) . '</p>';
                echo '</div>';
                echo '</div>';



                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo "<div  style='clear:both;'></div>";

                // вывод источника если разрешено
                // if($menuInfo->isResourceSet() and $siteContent->resource[$vId]){
                // 	echo "<span  class='small_font_2'>".$text_alert['resource'].": <span>".$siteContent->resource[$vId]."</span></span>";
                // }
                // вывод даты если разрешено

                // // вывод времени если разрешено
                if ($menuInfo->isTimeSet()) {
                    echo "<span class='small_font'>" . $text_alert['show_time'] . ": <span>" . $siteContent->time[$vId] . "</span></span>&nbsp;&nbsp;";
                }

                if ($menuInfo->isDetailLinkSet()) {
                    echo "<a href='?title=" . $siteContent->link[$vId] . "'>" . $text_alert['podrobnee'] . "</a>";
                }
                echo '</div>';
            }
            echo '</div>';
        }
        $file_to_include =  $siteInfo->dirfiles . "/pager_sys.php";
        if (file_exists($file_to_include) and !is_dir($file_to_include)) include($file_to_include);
        ?>
    </div>
    </div>
    </div>
    </div>
    </div>
    <?
    unset($showInfoState);
    ?>
    </div>
</main>
<?
$file_to_include = "./transform_files/Default_component__block.php";
if (file_exists($file_to_include) and !is_dir($file_to_include)) include($file_to_include);
?>